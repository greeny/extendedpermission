<?php

namespace Phoenix\Permission;

use Nette\Object,
    Nette\Database\Connection,
    Phoenix\Permission\InvalidConfigException;

/**
 *
 * @author Tomáš Blatný
 */
class Permission extends Object
{
    /** @var Connection */
    protected $connection;

    /** @var array */
    protected $config;

    /** @var \Phoenix\Permission\IAuthorizator */
    protected $authorizator;

    /** @var \Nette\Security\IAuthorizator */
    protected $nAuthorizator;

    public function __construct(array $config, Connection $connection, \Nette\Security\IAuthorizator $nAuthorizator = null)
    {
        if(!isset($config["permission"]))
        {
            throw new InvalidConfigException("Corrupted Phoenix\\Permission configuration, missing section 'permission' in 'parameters/phoenix'.");
        }

        $this->config = new Config($config["permission"]);
        $this->connection = $connection;
        $this->nAuthorizator = $nAuthorizator;

        $authorizator = $this->config->get("authorizator", "database");

        switch($authorizator)
        {
            //case "config": $this->authorizator = new ConfigAuthorizator($this->config); break;
            case "database": $this->authorizator = new DatabaseAuthorizator($this->connection, $this->config); break;
            case "php": break;
        }
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function isAllowed(IRole $role, IResource $resource, $privilege)
    {
        if($this->authorizator instanceof IAuthorizator)
        {
            return $this->authorizator->isAllowed($role, $resource, $privilege, $this->nAuthorizator);
        }
        else
        {
            throw new InvalidAuthorizatorException("Authorizator is not 'Phoenix\\Permission\\IAuthorizator'.");
        }
    }
}
