<?php

namespace Phoenix\Permission;

use Nette\Object;

/**
 *
 * @author Tomáš Blatný
 */

function c($object)
{
    return $object;
}

class Config extends Object
{
    protected $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param $name
     * @param int $default
     * @param null $config
     * @return Config | mixed
     */
    public function get($name, $default = 0, $config = null)
    {
        if(!$config)
        {
            $config = $this->config;
        }

        $items = explode("/", $name);
        if(count($items)>1)
        {
            if(!isset($config[$items[0]]))
            {
                return $default;
            }
            else
            {
                $item = $items[0];
                unset($items[0]);
                return c(new Config($config[$item]))->get(implode("/", $items), $default);
            }
        }
        else
        {
            if(isset($config[$name]))
            {
                return $config[$name];
            }
            else
            {
                return $default;
            }
        }
    }
}
