<?php

namespace Phoenix\Permission;

use Nette\Object;
use Nette\Database\Connection;

/**
 *
 * @author Tomáš Blatný
 */
class DatabaseAuthorizator extends Object implements IAuthorizator
{
    /** @var \Nette\Database\Connection */
    protected $c;

    /** @var array */
    protected $config;

    public function __construct(Connection $c, Config $config)
    {
        $this->c = $c;
        $this->config = $config->config;
    }

    public function isAllowed(IRole $role, IResource $resource, $privilege, \Nette\Security\IAuthorizator $nAuthorizator = null)
    {
        $ruid = $role->getUniqueId();
        $rgid = $role->getGeneralId();
        $euid = $resource->getUniqueId();
        $egid = $resource->getGeneralId();

        $rrow = $this->config["database"]["role"];
        $erow = $this->config["database"]["resource"];

        $default = ($this->config["default"]=="allow"?true:false);

        $perm = $this->c
            ->table($this->config["database"]["table"])
            ->where($this->config["database"]["role"], array($ruid, $rgid))
            ->where($this->config["database"]["resource"], array($euid, $egid))
            ->where($this->config["database"]["privilege"], $privilege);

        while($row = $perm->fetch())
        {
            $perms[$row->$rrow][$row->$erow] = true;
        }

        if(isset($perms[$ruid][$euid])) return !$default;
        else if(isset($perms[$ruid][$egid])) return !$default;
        else if(isset($perms[$rgid][$euid])) return !$default;
        else if(isset($perms[$rgid][$egid])) return !$default;
        else
        {
            if($nAuthorizator instanceof \Nette\Security\IAuthorizator)
            {
                foreach($role->getRoles() as $role)
                {
                    if($nAuthorizator->isAllowed($role, $resource->getResource(), $privilege))
                    {
                        return !$default;
                    }
                }
            }
            return $default;
        }
    }
}
