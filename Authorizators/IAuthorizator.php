<?php

namespace Phoenix\Permission;

use Phoenix\Permission\IRole;
use Phoenix\Permission\IResource;

/**
 *
 * @author Tomáš Blatný
 */
interface IAuthorizator
{
    public function isAllowed(IRole $role, IResource $resource, $privilege, \Nette\Security\IAuthorizator $nAuthorizator);
}
