<?php

namespace Phoenix\Permission;

use Nette\Object;
use Phoenix\Permission\IAuthorizator;

/**
 * data format: greeny: useradmin: usermodify
 * @author Tomáš Blatný
 */
class ConfigAuthorizator extends Object implements IAuthorizator
{
    /** @var \Phoenix\Permission\Config */
    protected $config;

    /** @var bool */
    protected $default = false;

    public function __construct(Config $config)
    {
        dump($this->config = $config->get("data/authorizator", array()));
        if(is_array($this->config))
        {
            throw new InvalidConfigException("Corrupted config file, excepted 'authorizator-data' section in section 'phoenix/permission'");
        }
        $default = $config->get("default", "deny");

        if($default == "allow")
        {
            $this->default = true;
        }
        else
        {
            $this->default = false;
        }
    }

    public function isAllowed(IRole $role, IResource $resource, $privilege)
    {
        $allow = $this->config->get($role->getUniqueId()."/".$resource->getUniqueId()."/".$privilege, $this->default);

        if($allow == $this->default)
        {
            return $this->default;
        }
        else
        {
            return !$this->default;
        }
    }
}
