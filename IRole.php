<?php

namespace Phoenix\Permission;

/**
 *
 * @author Tomáš Blatný
 */
interface IRole
{
    public function getUniqueId();
    public function getGeneralId();
    public function getRoles();
}
