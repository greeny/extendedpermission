<?php

namespace Phoenix\Permission;

/**
 *
 * @author Tomáš Blatný
 */
interface IResource
{
    public function getUniqueId();
    public function getGeneralId();
    public function getResource();
}
